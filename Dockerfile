#FROM maven:3.5.3-jdk-10
FROM gitlab.velacorp.vn:4567/shippo/sync-service/base/delivery-sync

#Install crontab
RUN apt-get update -y && apt-get install cron -y
COPY ./crontab/crontab /etc/cron.d/delete-cron
RUN chmod +x /etc/cron.d/delete-cron
RUN crontab /etc/cron.d/delete-cron

# Install project dependencies and keep sources
# make source folder
RUN mkdir -p /data/app/src
RUN mkdir -p /data/app/deploy/delivery-background/libs
RUN mkdir -p /data/app/deploy/delivery-background/conf

# install maven dependency packages (keep in image)
COPY .. /data/app/src
WORKDIR /data/app/src
# RUN rm -rf target
RUN mvn clean
#RUN mvn compile package
RUN mvn compile package
RUN ls /data/app/src/target/
RUN cp  /data/app/src/target/*.jar /data/app/deploy/delivery-background/libs
RUN cp -r /data/app/src/target/libs/*.jar /data/app/deploy/delivery-background/libs
RUN ls /data/app/deploy/delivery-background/libs
COPY ./conf /data/app/deploy/delivery-background/conf
RUN ls /data/app/deploy/delivery-background/conf
WORKDIR /data/app/deploy/delivery-background
ARG APP_HOME=/data/app/deploy/delivery-background
ENV APP_HOME=$APP_HOME
ARG JAVA_OPTS="-Dapp.home=$APP_HOME"
ENV JAVA_OPTS=$JAVA_OPTS
CMD cron && java -cp "$APP_HOME/conf:$APP_HOME/libs/*" shippo.main.DeliveryBackgroundHandlingApp