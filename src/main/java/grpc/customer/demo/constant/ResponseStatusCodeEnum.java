package grpc.customer.demo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter

public enum  ResponseStatusCodeEnum {
    SUCCESS("200", "success"),
    BAD_REQUEST("400", "bad request"),
    BALANCE_ERROR("IFI3000", "Balance not enough");

    private  String code;
    private String message;

    @Override
    public String toString() {
        return "ResponseStatusCodeEnum{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
