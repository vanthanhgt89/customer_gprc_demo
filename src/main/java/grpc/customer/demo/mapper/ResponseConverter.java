package grpc.customer.demo.mapper;

import grpc.customer.demo.CreateCustomerOrderResponse;
import grpc.customer.demo.entity.BO.StatusBO;
import org.springframework.stereotype.Service;

@Service
public interface ResponseConverter {
    CreateCustomerOrderResponse createOrder(StatusBO obj);
}
