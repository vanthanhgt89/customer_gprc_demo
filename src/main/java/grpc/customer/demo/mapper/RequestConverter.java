package grpc.customer.demo.mapper;

import grpc.customer.demo.CreateCustomerOrderRequest;
import grpc.customer.demo.entity.BO.OrderBO;
import org.springframework.stereotype.Service;

@Service
public interface RequestConverter {
    OrderBO createOrder(CreateCustomerOrderRequest obj);
}
