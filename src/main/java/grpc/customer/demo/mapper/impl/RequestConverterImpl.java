package grpc.customer.demo.mapper.impl;

import grpc.customer.demo.CreateCustomerOrderRequest;
import grpc.customer.demo.entity.BO.OrderBO;
import grpc.customer.demo.mapper.RequestConverter;
import org.springframework.stereotype.Service;

@Service
public class RequestConverterImpl implements RequestConverter {

    @Override
    public OrderBO createOrder(CreateCustomerOrderRequest obj) {
        return new OrderBO(obj.getRequest().getCustomerId(), obj.getRequest().getOrderName(), obj.getRequest().getValue());
    }

}
