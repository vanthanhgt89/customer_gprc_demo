package grpc.customer.demo.mapper.impl;

import grpc.customer.demo.CreateCustomerOrderResponse;
import grpc.customer.demo.Status;
import grpc.customer.demo.entity.BO.StatusBO;
import grpc.customer.demo.mapper.ResponseConverter;
import org.springframework.stereotype.Service;

@Service
public class ResponseConverterImpl implements ResponseConverter {

    @Override
    public CreateCustomerOrderResponse createOrder(StatusBO obj) {
        return CreateCustomerOrderResponse.newBuilder()
                                            .setResponse(
                                                Status.newBuilder()
                                                        .setCode(obj.getCode())
                                                        .setMessage(obj.getMessage())
                                                        .build()
                                        )
                                        .build();
    }

}
