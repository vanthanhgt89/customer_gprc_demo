package grpc.customer.demo.controller;

import grpc.customer.demo.entity.CustomerEntity;
import grpc.customer.demo.factory.ResponseFactory;
import grpc.customer.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ResponseFactory responseFactory;

    @GetMapping("/api/v1/customers")
    public ResponseEntity<?> getCustomer(){
        List<CustomerEntity> customerEntityList = customerService.findAll();

        return responseFactory.success(customerEntityList, List.class);

    }

    @GetMapping("/api/v1/customers/{id}")
    public ResponseEntity getCustomerById(@PathVariable Integer id){
        CustomerEntity customer = customerService.findCustomerById(id);
        return  responseFactory.success(customer, CustomerEntity.class);
    }
}
