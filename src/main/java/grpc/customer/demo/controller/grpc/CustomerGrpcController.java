package grpc.customer.demo.controller.grpc;

import grpc.customer.demo.*;
import grpc.customer.demo.entity.BO.OrderBO;
import grpc.customer.demo.entity.BO.StatusBO;
import grpc.customer.demo.mapper.RequestConverter;
import grpc.customer.demo.mapper.ResponseConverter;
import grpc.customer.demo.service.CustomerService;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


@GRpcService
public class CustomerGrpcController extends CustomerServiceGrpc.CustomerServiceImplBase {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private ResponseConverter responseConverter;

    @Autowired
    private RequestConverter requestConverter;


    @Override
    @Transactional
    public void createCustomerOrder(CreateCustomerOrderRequest request, StreamObserver<CreateCustomerOrderResponse> responseObserver) {
        OrderBO orderBO = requestConverter.createOrder(request);
        StatusBO status = customerService.updateCustomer(orderBO);
        if(status != null){
            CreateCustomerOrderResponse response = responseConverter.createOrder(status);
            responseObserver.onNext(response);
        }else {
            responseObserver.onError(new RuntimeException("Customer service error"));
        }
        responseObserver.onCompleted();
    }
}
