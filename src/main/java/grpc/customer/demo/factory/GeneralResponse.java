package grpc.customer.demo.factory;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GeneralResponse<T> implements Serializable {
    @JsonProperty("status")
    private ResponseStatus responseStatus;

    @JsonProperty("data")
    private T data;

    @Override
    public String toString() {
        return "GeneralResponse{" +
                "responseStatus=" + responseStatus +
                ", data=" + data +
                '}';
    }
}
