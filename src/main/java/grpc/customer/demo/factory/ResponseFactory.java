package grpc.customer.demo.factory;

import grpc.customer.demo.constant.ResponseStatusCodeEnum;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ResponseFactory {
    public ResponseEntity<?> success(Object data, Class<?> classz){
        GeneralResponse generalResponse = new GeneralResponse<>();
        ResponseStatus responseStatus = new ResponseStatus();
        responseStatus.setCode(ResponseStatusCodeEnum.SUCCESS.getCode());
        responseStatus.setMessage(ResponseStatusCodeEnum.SUCCESS.getMessage());
        generalResponse.setResponseStatus(responseStatus);
        generalResponse.setData(classz.cast(data));
        return  ResponseEntity.ok(generalResponse);
    }


}
