package grpc.customer.demo.service;

import grpc.customer.demo.constant.ResponseStatusCodeEnum;
import grpc.customer.demo.entity.BO.OrderBO;
import grpc.customer.demo.entity.BO.StatusBO;
import grpc.customer.demo.entity.CustomerEntity;
import grpc.customer.demo.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private CustomerRepository customerRepository;

    public List<CustomerEntity> findAll(){
        return customerRepository.findAll();
    }

    public CustomerEntity findCustomerById(Integer id){
        Optional<CustomerEntity> customer = customerRepository.findById(id);
        if(customer.isPresent()){
            CustomerEntity entity = customer.get();
            return entity;
        }
        return null;
    }

    // GRPC
    public StatusBO updateCustomer(OrderBO orderBO){
        Optional<CustomerEntity> customer = customerRepository.findById(orderBO.getCustomerId());
        if(customer.isPresent()){
            logger.info("======  Customer id {} =======", orderBO.getCustomerId());
            CustomerEntity entity = customer.get();
            if(orderBO.getValue() <= entity.getBalance()){
                logger.info("===== create order balance {} =====", orderBO.getValue());
                Double remainBalance = entity.getBalance() - orderBO.getValue();
                entity.setBalance(remainBalance);
                CustomerEntity updated = customerRepository.save(entity);
                if(updated != null){
                    return new StatusBO(ResponseStatusCodeEnum.SUCCESS.getCode(), ResponseStatusCodeEnum.SUCCESS.getMessage());
                }
            }else {
                return new StatusBO(ResponseStatusCodeEnum.BALANCE_ERROR.getCode(), ResponseStatusCodeEnum.BALANCE_ERROR.getMessage());
            }

        }
        return null;
    }

}
