package grpc.customer.demo.entity.BO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter

public class OrderBO {
    private Integer customerId;
    private String orderName;
    private Double value;
}
