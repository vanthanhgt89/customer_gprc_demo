package grpc.customer.demo.entity.BO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter

public class StatusBO {
    private String code;
    private String message;
}
